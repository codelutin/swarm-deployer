# swarm-deployer

Le but est de permettre de soumettre des demandes de deploiement de fichier
docker-compose.yml ainsi que des fichiers de configuration et des secrets.

Une fois soumis, un admin peu valider la demander pour que le deploiement
soit effectif. Si c'est un admin qui soumet il peut demander le déploiement
automatiquement.

Lors de la soumission, on doit indiqué la cible, si le compose essaie de
deployer autre part, la demande est rejeté (pas implanter pour l'instant)

# Configuration simple

* `DATABASE_URL`: URL de la base de données (ex: `postgres://dbuser:xxxxxxxx@localhost:5432/swarm-deployer`)
* `SECRET_KEY_FILE`: Chemin du fichier contenant le secret pour le chiffrement JWT
* `SECRET_KEY`: Secret pour le chiffrement JWT (est prioritaire sur `SECRET_KEY_FILE`)
* `REGISTRY_*`: Chaque variable commençant par `REGISTRY_` représente un registry Docker à utiliser. Ex: `REGISTRY_MONDOMAINE=registry.mondomaine.tld monlogin monpassword`

# Configuration avancée

* `CONFIG_REGISTRY_PREFIX`: Préfixe des variables ou des fichiers contenant des registries (valeur par default: `REGISTRY`)(ex: `REGISTRY_NUITON=registry.nuiton.org monlogin monpassword`)
* `CONFIG_REGISTRY_DIR`: Répertoire contenant des fichiers avec des registries (un par ligne, au format `<url> <login> <password>`). Seuls les fichiers commençant par `$CONFIG_REGISTRY_PREFIX` sont lus.
* (WIP) `SSH_KEY_FILE`: Chemin de la clé privée pour l'accès ssh au repo Prometheus
* (WIP) `PROMETHEUS_REPO`: URI de connexion au repo Prometheus (ex: `ssh://prom@prom.codelutin.com:2222/etc/prom/swarm-deployer-config`)
* (WIP) `PROMETHEUS_CONFIG_NAME`: Nom du fichier Prometheus a rechercher dans le zip de déploiement, seuls les `.yaml` sont supportés (valeur par default: `swarm-deployer.prom.yaml`)

# Ajout de comptes

Il faut se connecter à Postgres, l'ajout est manuel pour le moment.

## Compte pour un service

```sql
insert into deployer (email, password, token, admin) values ('nom-du-service@mondomaine.tld', '', gen_random_uuid(), false);
```

(en insérant un mdp vide, le login par mdp sera désactivé)

## Compte pour une personne

### Hachage du mot de passe

swarm-deployer a besoin d'un mot de passe haché avec bcrypt. Il existe de [multiples méthodes](https://unix.stackexchange.com/questions/307994/compute-bcrypt-hash-from-command-line) pour calculer le hash, voici 2 exemples:

* Sur des distros Debian ou Red-Hat like: `mkpasswd -m bcrypt -R 15` (il faut installer le paquet `whois` ou `mkpasswd` respectivement)
* Sur d'autres distros, installer le paquet Python `bcrypt` (sur Arch par exemple, c'est le paquet système `python-bcrypt`), puis lancer `python -c 'import bcrypt; print(bcrypt.hashpw(b"PASSWORD", bcrypt.gensalt(rounds=15)).decode("utf-8"))'`

Dans les deux cas, le hash obtenu devrait ressembler à `$2b$15$F1CKNGAfSS3Ph8kIPUtTBeeG/evCcaBbL4mZYHJggebkSu8knM9uu`

### Ajout dans la base de données

```sql
insert into deployer (email, password, token, admin) values ('<mail_de_la_personne>', '<hash>', gen_random_uuid(), false);
```


# Probleme de dependance docker-cli

par defaut il telechargeait la version 17.3, il faut forcer avec
go get -u "github.com/docker/cli@v0.0.0-20190131223713-234462756460"
pour connaitre la version a mettre on peut aller sur
https://index.golang.org/index

# TODO

* Remplacer gorilla/mux, qui a été [déprécié](https://github.com/gorilla#gorilla-toolkit)
* Authentification OAUTH ?
  * Voir https://stackoverflow.com/questions/48855122/keycloak-adaptor-for-golang-application/51456282#51456282
  * Exemple de conf:
  ```
  OAUTH_SERVER=https://auth.cloud.codelutin.com/auth/realms/codelutin
  OAUTH_CLIENT_ID=swarm-deployer
  OAUTH_CLIENT_SECRET=xxxxx
  ```
* Authentification LDAP ?
