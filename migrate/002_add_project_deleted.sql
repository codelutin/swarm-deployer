-- permet de marque un projet comme supprimé (non visible de l'ui), mais sans le retirer de la base (tracabilité)
ALTER TABLE project ADD deleted BOOLEAN DEFAULT false;

CREATE INDEX project_deleted_idx ON project (deleted);