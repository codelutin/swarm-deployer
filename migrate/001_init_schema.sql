CREATE EXTENSION IF NOT EXISTS "pgcrypto";

CREATE TABLE deployer (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    updateDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    email TEXT NOT NULL,
    password TEXT NOT NULL,
    token TEXT,
    admin boolean, -- si vrai peut faire les deploy sinon, ne peut que soumettre une demande
    active boolean -- si false le compte ne peut plus servir
);

CREATE UNIQUE INDEX deployer_email_idx ON deployer (email);
CREATE INDEX deployer_token_idx ON deployer (token);

CREATE TABLE project (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    updateDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    submitter UUID REFERENCES deployer(id), -- pas de cascade, on bloque la suppression des users
    name TEXT NOT NULL,  -- la stack portera comme nom <project>-<flavour>
    flavour TEXT NOT NULL,  -- prod, ramonville, branch-toto, tag-titi, ...
    info TEXT, -- zone d'informations diverses
    target TEXT NOT NULL, -- infracl, infrasaas, demo
    composeFile Text NOT NULL, -- le nom du fichier compose a utiliser (qui se trouve dans le zip)
    zip TEXT -- fichier zip contenant tous les fichiers necessaires (compose, configs, secrets, ...)
);

CREATE INDEX project_name_idx ON project (name);
CREATE INDEX project_submitter_idx ON project (submitter);

CREATE TABLE deployment (
    id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
    creationDate TIMESTAMP  WITH TIME ZONE DEFAULT current_timestamp,
    project UUID REFERENCES project(id), -- pas de cascade, on bloque la suppression des projects
    deployer UUID REFERENCES deployer(id), -- pas de cascade, on bloque la suppression des users
    output TEXT -- le contenu de stdout et stderr suite à la demande de déploiement
);

CREATE INDEX deployment_project_idx ON deployment (project);
CREATE INDEX deployment_deployer_idx ON deployment (deployer);

-- Mise a jour automatique de la date de derniere modification
CREATE OR REPLACE FUNCTION update_creationDate_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.creationDate = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION update_updateDate_column()
RETURNS TRIGGER AS $$
BEGIN
    NEW.updateDate = now();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE TRIGGER update_deployer_createDate BEFORE INSERT ON deployer FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();
CREATE TRIGGER update_project_createDate BEFORE INSERT ON project FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();
CREATE TRIGGER update_deployment_createDate BEFORE INSERT ON deployment FOR EACH ROW EXECUTE PROCEDURE update_creationDate_column();

CREATE TRIGGER update_deployer_updateDate BEFORE INSERT OR UPDATE ON deployer FOR EACH ROW EXECUTE PROCEDURE update_updateDate_column();
CREATE TRIGGER update_project_updateDate BEFORE INSERT OR UPDATE ON project FOR EACH ROW EXECUTE PROCEDURE update_updateDate_column();

---- create above / drop below ----

DROP TABLE deployer;
DROP TABLE deployment;

DROP FUNCTION update_creationDate_column;
DROP FUNCTION update_updateDate_column;

DROP EXTENSION IF EXISTS "pgcrypto";
