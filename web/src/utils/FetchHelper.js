let FetchHelper = {
  // pas vraiment la bonne place pour cette methode
  stackName(project) {
    return `${project.target}--${project.name}--${project.flavour}`
  },

  createUrl(url) {
    return window.BACKEND_URL + 'api/v1' + url
  },

  async fetch(url, method, headers, body) {
    headers = headers || {}
    if (!headers['Content-Type']) {
      headers['Content-Type'] = 'application/json'
    }

    return fetch(this.createUrl(url), {
      headers,
      method,
      credentials: 'include', // en prod a priori le back et le front auront la meme origine donc pas besoin
      redirect: 'follow',
      mode: 'cors',
      body: typeof body === 'string' ? body : body && JSON.stringify(body)
    }).then(response => {
      if (response.status === 204) {
        return null
      }

      if (response.status === 200 || response.status === 201) {
        if ((response.headers.get('Content-Type') || '').toLowerCase() === 'application/json') {
          return response.json()
        } else if (parseInt(response.headers.get('Content-Length')) > 0) {
          return response.text()
        } else {
          return null
        }
      }

      if (response.status === 503) {
        return Promise.reject()
      }

      return response.text().then(
        cause => {
          return Promise.reject({ status: response.status, cause })
        },
        () => {
          return Promise.reject({})
        }
      )
    })
  },

  get(url) {
    return this.fetch(url, 'GET')
  },

  post(url, body) {
    return this.fetch(url, 'POST', {}, body)
  },

  put(url, body) {
    return this.fetch(url, 'PUT', {}, body)
  },

  patch(url, body) {
    return this.fetch(url, 'PATCH', {}, body)
  },

  delete(url) {
    return this.fetch(url, 'DELETE')
  }
}

export default FetchHelper
