import Vue from 'vue'
import App from './App.vue'
import router from './router'
import FetchHelper from '@/utils/FetchHelper.js'
import Component from 'vue-class-component'
import StoreHelper from './utils/Store'

// Register the router hooks with their names
Component.registerHooks([
  'beforeRouteEnter',
  'beforeRouteLeave',
  'beforeRouteUpdate'
])

window.BACKEND_URL = process.env.VUE_APP_BACKEND_URL
window.FRONTEND_URL = process.env.BASE_URL

// register utilities function on window, Vue and Vue instance
if (typeof window !== 'undefined') {
  window.$fetch = FetchHelper
  window.$store = StoreHelper
}

Vue.$fetch = FetchHelper
Vue.$store = StoreHelper

Vue.config.productionTip = false
Vue.prototype.$fetch = FetchHelper
Vue.prototype.$store = StoreHelper

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
