package main

import (
	"log"
	"os"

	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/http"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/repository"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"
)

func main() {
	databaseURL := os.Getenv("DATABASE_URL")
	secretKeyFile := os.Getenv("SECRET_KEY_FILE")
	secretKey := os.Getenv("SECRET_KEY")
	sshKeyFile := os.Getenv("SSH_KEY_FILE")
	prometheusRepoURI := os.Getenv("PROMETHEUS_REPO")
	prometheusConfigName := os.Getenv("PROMETHEUS_CONFIG_NAME")
	configRegistryPrefix := os.Getenv("CONFIG_REGISTRY_PREFIX") // use for variable and file
	configRegistryDirectory := os.Getenv("CONFIG_REGISTRY_DIR")

	if secretKey == "" {
		if secretKeyFile != "" {
			content, err := utils.ReadFile(secretKeyFile)
			if err != nil {
				log.Fatal("Can't read secrect key file", secretKeyFile)
			}
			secretKey = content
		}
	}

	if secretKey == "" {
		// for dev only
		log.Fatal("Env variable SECRET_KEY or SECRET_KEY_FILE must be used")
	}

	if configRegistryPrefix == "" {
		configRegistryPrefix = "REGISTRY"
	}

	log.Println("Init Docker")
	utils.InitDocker(configRegistryPrefix, configRegistryDirectory)

	log.Println("Init ssh client")
	utils.InitSSHClient(prometheusRepoURI, sshKeyFile, prometheusConfigName)

	log.Println("Init database")
	repository.Init(databaseURL, true)
	utils.JwtInit([]byte(secretKey))

	addr := ":23770"
	log.Println("Start web server", addr)
	http.Start(addr)
}
