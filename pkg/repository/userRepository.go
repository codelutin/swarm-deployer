package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"

	"github.com/jackc/pgtype"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/constant"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"
)

/*
UserJSON return user in json format
all field are send except:
- password
- email confirmation token
*/
func UserJSON(currentUser model.User, id string, fields ...string) (string, error) {
	var allFields string
	if len(fields) == 0 {
		allFields = "id, creationdate, updatedate, email, token, admin, active"
	} else {
		allFields = strings.Join(fields, ",")
	}

	q := &query{sql: fmt.Sprintf(`WITH __all AS (select %s FROM deployer where id=$1) SELECT json_agg(__all.*) as j FROM __all`, allFields)}
	result, err := q.QueryString(currentUser, id)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	result = result[1 : len(result)-1] // suppression des []

	return result, nil
}

/*
UserIDFromToken get user id  by application token
*/
func UserFromToken(token string, fields ...string) (model.User, error) {
	currentUser := constant.Nobody

	var user model.User

	var allFields string
	if len(fields) == 0 {
		allFields = "id, creationdate, updatedate, email, token, admin, active"
	} else {
		allFields = strings.Join(fields, ",")
	}

	q := &query{sql: fmt.Sprintf(`WITH __all AS (SELECT %s as j FROM deployer where token=$1) SELECT json_agg(__all.*) FROM __all`, allFields)}
	result, err := q.QueryString(currentUser, token)
	if err != nil {
		return user, utils.NewHTTPError500(err, currentUser)
	}

	result = result[1 : len(result)-1] // suppression des []
	err = json.Unmarshal([]byte(result), &user)
	if err != nil {
		return user, utils.NewHTTPError500(err, currentUser)
	}

	return user, nil
}

/*
CheckUserPasswordForID check password for id
*/
func CheckUserPasswordForID(id string, password string) bool {
	var hash string
	row := db.QueryRow(context.Background(), `select password from deployer where id=$1`, id)
	err := row.Scan(&hash)
	if err != nil {
		return false
	}

	return utils.CheckPassword(password, hash)
}

/*
CheckUserPasswordForEmail retourne l'id de l'utilisateur (string) si le mot de passe est le bon et que l'utilisateur est retrouve
*/
func CheckUserPasswordForEmail(email string, password string) (string, error) {
	var uuid pgtype.UUID
	var hash string
	row := db.QueryRow(context.Background(), `select id, password from deployer where email=$1`, email)
	err := row.Scan(&uuid, &hash)
	if err != nil {
		return "", utils.NewHTTPError500(err, constant.Nobody)
	}

	if !utils.CheckPassword(password, hash) {
		return "", utils.NewHTTPError(fmt.Sprintf("Password mismatch for %s", email), constant.Nobody, 401)
	}

	var result string
	err = uuid.AssignTo(&result)
	if err != nil {
		return "", utils.NewHTTPError500(err, constant.Nobody)
	}

	return result, nil
}

/*
CreateUser retourne l'utilisateur au format json
*/
func CreateUser(email string, password string, admin bool) (string, error) {
	hashPassword, err := utils.HashPassword(password)
	if err != nil {
		return "", utils.NewHTTPError500(err, model.User{ID: email})
	}

	uuid, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, model.User{ID: email})
	}

	token, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, model.User{ID: email})
	}

	currentUser := model.User{
		ID: uuid, Email: email, Password: hashPassword, Token: token, Admin: admin, Active: true}
	userAsJSON, err := json.Marshal(currentUser)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	log.Println("create user", string(userAsJSON))

	q := &query{sql: `INSERT INTO deployer AS t SELECT * FROM json_populate_record(NULL::Deployer, $1::json);`}
	err = q.execOnOneRow(currentUser, userAsJSON)

	return currentUser.ID, err
}

/*
UpdateUserPassword update user password, if old password match, or if force is true
*/
func UpdateUserPassword(currentUser model.User, id string, password string, oldPassword string, force bool) error {
	if !force && !CheckUserPasswordForID(id, oldPassword) {
		return utils.NewHTTPError(fmt.Sprintf("Bad old password for user '%s'", id), currentUser, 400)
	}

	hash, err := utils.HashPassword(password)
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	q := &query{sql: `update deployer SET password=$2 where id=$1`}
	err = q.execOnOneRow(currentUser, id, hash)

	return err
}

/*
ResetUserToken ajout un token d'authentification pour l'utilisateur
*/
func ResetUserToken(currentUser model.User, id string) (string, error) {
	token, err := utils.GenUUID()
	if err != nil {
		return "", err
	}

	q := &query{sql: `update deployer SET token=$2 where id=$1;`}
	err = q.execOnOneRow(currentUser, id, token)

	return token, err
}

// TODO
// ProjectName
// Flavour (project):
// deployment (project)
