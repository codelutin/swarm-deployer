package repository

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"
)

func Project(currentUser model.User, id string) (model.Project, error) {
	q := &query{sql: `SELECT row_to_json(p.*) FROM project p WHERE id=$1`}
	result, err := q.QueryString(currentUser, id)

	var project model.Project
	if err != nil {
		return project, utils.NewHTTPError500(err, currentUser)
	}

	err = json.Unmarshal([]byte(result), &project)
	if err != nil {
		return project, utils.NewHTTPError500(err, currentUser)
	}

	return project, nil
}

func op(s string) string {
	if s == "" {
		return "!="
	}
	return "="
}

// [{id, name, flavour, creationDate, submitter, info, target, deployments: [{id, creationDate, deployer, output}]}]
func ProjectsJSON(currentUser model.User, target string, name string, flavour string) (string, error) {
	log.Println("ProjectsJSON", currentUser, target, name, flavour, "...")
	where := fmt.Sprintf(`target%v$1 AND name%v$2 AND flavour%v$3`, op(target), op(name), op(flavour))
	q := &query{sql: fmt.Sprintf(`
	  WITH projects AS (SELECT p.id, name, flavour, p.creationDate, u.email AS submitter, info, target FROM project p LEFT JOIN deployer u ON p.submitter = u.id WHERE deleted != true AND %v order by target, name, flavour, creationDate DESC)
	        , __all AS (SELECT p.*, (SELECT json_agg(depFrom) AS deployments FROM (select d.creationDate, u.email AS deployer, output FROM deployment d LEFT JOIN deployer u ON d.deployer = u.id WHERE d.project = p.id ORDER BY d.creationDate DESC) AS depFrom) FROM projects p)
	  SELECT json_agg(__all.*) as j FROM __all`, where)}
	result, err := q.QueryString(currentUser, target, name, flavour)

	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

func ProjectDelete(currentUser model.User, id string) error {
	log.Printf("Suppression du project %s\n", id)
	q := &query{sql: `UPDATE project SET deleted=true WHERE id=$1;`}
	err := q.execOnOneRow(currentUser, id)
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	return err
}

// [{name: string, flavours: [string]}]
func ProjectsFlavourJSON(currentUser model.User) (string, error) {
	q := &query{sql: `WITH __all AS (select name, json_agg(distinct flavour) as flavours from project group by name order by name) SELECT json_agg(__all.*) as j FROM __all`}
	result, err := q.QueryString(currentUser)

	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

func CreateProject(currentUser model.User, project model.Project) (string, error) {

	id, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	project.ID = id
	project.Submitter = currentUser.ID
	project.CreationDate = time.Now()
	project.UpdateDate = project.CreationDate

	projectAsJSON, err := json.Marshal(project)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	log.Printf("Creation du project %s(%s) in '%s' by %s (%s)\n", project.Name, project.Flavour, project.Target, project.Submitter, project.Info)
	q := &query{sql: `INSERT INTO project AS t SELECT * FROM json_populate_record(NULL::project, $1::json);`}
	err = q.execOnOneRow(currentUser, string(projectAsJSON))
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return id, err
}

func CreateDeployment(currentUser model.User, deployment model.Deployment) (string, error) {
	id, err := utils.GenUUID()
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	deployment.ID = id
	deployment.Deployer = currentUser.ID
	deployment.CreationDate = time.Now()

	asJSON, err := json.Marshal(deployment)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	log.Printf("Creation du deployment %s\n", asJSON)
	q := &query{sql: `WITH __all AS (INSERT INTO deployment AS t SELECT * FROM json_populate_record(NULL::deployment, $1::json) RETURNING *) SELECT ROW_TO_JSON(__all.*) FROM __all;`}
	result, err := q.QueryString(currentUser, string(asJSON))
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, err

}
