package repository

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jackc/tern/migrate"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"
)

var db *pgxpool.Pool

type errorLineExtract struct {
	LineNum   int    // Line number starting with 1
	ColumnNum int    // Column number starting with 1
	Text      string // Text of the line without a new line character.
}

/*
Init initialise la connexion a la base en utilisant
*/
func Init(databaseURL string, doMigration bool) {
	if doMigration {
		migrateDatabase(databaseURL)
	}

	poolConfig, err := pgxpool.ParseConfig(databaseURL)
	if err != nil {
		log.Fatalln("Unable to parse DATABASE_URL", "error", err)
	}

	// on utilise après la migration de base l'utilisateur nobody
	// FIXME
	// poolConfig.ConnConfig.Password = "nobody"

	ctx := context.Background()
	db, err = pgxpool.ConnectConfig(ctx, poolConfig)
	if err != nil {
		log.Fatalln("Unable to create connection pool", databaseURL, "error", err)
	}

}

// extractErrorLine takes source and character position extracts the line
// number, column number, and the line of text.
//
// The first character is position 1.
func extractErrorLine(source string, position int) (errorLineExtract, error) {
	ele := errorLineExtract{LineNum: 1}

	if position > len(source) {
		return ele, fmt.Errorf("position (%d) is greater than source length (%d)", position, len(source))
	}

	lines := strings.SplitAfter(source, "\n")
	for _, ele.Text = range lines {
		if position-len(ele.Text) < 1 {
			ele.ColumnNum = position
			break
		}

		ele.LineNum++
		position -= len(ele.Text)
	}

	ele.Text = strings.TrimSuffix(ele.Text, "\n")

	return ele, nil
}

func migrateDatabase(databaseURL string) {
	ctx := context.Background()

	dbConfig, err := pgx.ParseConfig(databaseURL)
	if err != nil {
		log.Fatalln("Unable to parse DATABASE_URL", "error", err)
	}

	conn, err := pgx.ConnectConfig(ctx, dbConfig)
	if err != nil {
		log.Fatalf("Error initializing database connexion:\n  %v\n", err)
	}
	defer conn.Close(ctx)

	migrator, err := migrate.NewMigrator(ctx, conn, "public.schema_version")
	if err != nil {
		log.Fatalf("Error initializing migrator:\n  %v\n", err)
	}

	err = migrator.LoadMigrations("migrate")
	if err != nil {
		log.Fatalf("Error loading migrations:\n  %v\n", err)
	}
	if len(migrator.Migrations) == 0 {
		log.Fatalf("No migrations found")
	}

	migrator.OnStart = func(sequence int32, name, direction, sql string) {
		log.Printf("%s executing %s %s\n%s\n\n", time.Now().Format("2006-01-02 15:04:05"), name, direction, sql)
	}

	var currentVersion int32
	currentVersion, err = migrator.GetCurrentVersion(ctx)
	if err != nil {
		log.Fatalf("Unable to get current version:\n  %v\n", err)
	}
	log.Printf("Migrate database from %d to last version\n", currentVersion)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt)
	go func() {
		<-interruptChan
		cancel()       // Cancel any in progress migrations
		signal.Reset() // Only listen for one interrupt. If another interrupt signal is received allow it to terminate the program.
	}()

	err = migrator.Migrate(ctx)

	if err != nil {
		log.Println(err)

		if err, ok := err.(migrate.MigrationPgError); ok {
			if err.Detail != "" {
				log.Println("DETAIL:", err.Detail)
			}

			if err.Position != 0 {
				ele, err := extractErrorLine(err.Sql, int(err.Position))
				if err != nil {
					log.Fatalf("Can't find line error:\n  %v\n", err)
				}

				prefix := fmt.Sprintf("LINE %d: ", ele.LineNum)
				log.Printf("%s%s\n", prefix, ele.Text)

				padding := strings.Repeat(" ", len(prefix)+ele.ColumnNum-1)
				log.Printf("%s^\n", padding)
			}
		}
		os.Exit(1)
	}
}

type query struct {
	presql  string
	sql     string
	postsql string
}

func (q *query) setPreSQL(sql string, arguments ...interface{}) {
	q.presql = fmt.Sprintf(sql, arguments...)
}

func (q *query) setSQL(sql string, arguments ...interface{}) {
	q.sql = fmt.Sprintf(sql, arguments...)
}

func (q *query) setPostSQL(sql string, arguments ...interface{}) {
	q.postsql = fmt.Sprintf(sql, arguments...)
}

func (q *query) execOnOneRow(currentUser model.User, arguments ...interface{}) error {
	tx, err := db.Begin(context.Background())
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}
	defer tx.Rollback(context.Background())

	if q.presql != "" {
		_, err = tx.Exec(context.Background(), q.presql, pgx.QuerySimpleProtocol(true))
		if err != nil {
			return utils.NewHTTPError500(err, currentUser)
		}
	}

	modif, err := tx.Exec(context.Background(), q.sql, arguments...)

	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	if modif.RowsAffected() != 1 {
		return utils.NewHTTPError(fmt.Sprintf("User not found '%v'", arguments), currentUser, 404)
	}

	if q.postsql != "" {
		_, err = tx.Exec(context.Background(), q.postsql, pgx.QuerySimpleProtocol(true))
		if err != nil {
			return utils.NewHTTPError500(err, currentUser)
		}
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return utils.NewHTTPError500(err, currentUser)
	}

	return nil
}

/*
QueryString fait une requete qui ne retourne qu'une seul ligne et retourne du string (qui peut être du json)
*/
func (q *query) QueryString(currentUser model.User, arguments ...interface{}) (string, error) {
	tx, err := db.Begin(context.Background())
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}
	defer tx.Rollback(context.Background())

	if q.presql != "" {
		_, err = tx.Exec(context.Background(), q.presql, pgx.QuerySimpleProtocol(true))
		if err != nil {
			return "", utils.NewHTTPError500(err, currentUser)
		}
	}

	row := tx.QueryRow(context.Background(), q.sql, arguments...)

	var pgjson pgtype.JSON
	err = row.Scan(&pgjson)
	if err != nil {
		return "", utils.NewHTTPError(fmt.Sprintf("Can't execute query '%s' %s (%v)", RemoveTabReturn(q.sql), argToString(arguments...), err), currentUser, 500)
	}

	if q.postsql != "" {
		_, err = tx.Exec(context.Background(), q.postsql, pgx.QuerySimpleProtocol(true))
		if err != nil {
			return "", utils.NewHTTPError500(err, currentUser)
		}
	}

	err = tx.Commit(context.Background())
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	var result string
	if pgjson.Status != pgtype.Present {
		return "", utils.NewHTTPError("Not found", currentUser, 404)
	}

	err = pgjson.AssignTo(&result)
	if err != nil {
		return "", utils.NewHTTPError500(err, currentUser)
	}

	return result, nil
}

func argToString(arguments ...interface{}) string {
	result := ""
	if len(arguments) > 0 {
		var argstring = make([]string, len(arguments))
		for i, arg := range arguments {
			argstring[i] = fmt.Sprintf("'%v'", arg)
		}
		result = "with arguments: " + strings.Join(argstring, ", ")
	}
	return result
}

/*
RemoveTabReturn remove all \n and \t in the string
*/
func RemoveTabReturn(s string) string {
	return strings.Map(func(c rune) rune {
		if c == '\n' || c == '\t' {
			return -1
		}
		return c
	}, s)
}
