package model

import (
	"time"
)

/*
User un utilisateur
*/
type User struct {
	ID           string    `json:"id,omitempty"`
	CreationDate time.Time `json:"creationdate,omitempty"`
	UpdateDate   time.Time `json:"updatedate,omitempty"`
	Email        string    `json:"email,omitempty"`
	Password     string    `json:"password,omitempty"`
	Token        string    `json:"token,omitempty"`
	Admin        bool      `json:"admin,omitempty"`
	Active       bool      `json:"active,omitempty"`
}

// Project un projet
type Project struct {
	ID           string    `json:"id,omitempty"`
	CreationDate time.Time `json:"creationdate,omitempty"`
	UpdateDate   time.Time `json:"updatedate,omitempty"`
	Deleted      bool      `json:"deleted"`
	Submitter    string    `json:"submitter,omitempty"`
	Name         string    `json:"name,omitempty"`
	Flavour      string    `json:"flavour,omitempty"`
	Info         string    `json:"info,omitempty"`
	Target       string    `json:"target,omitempty"`
	ComposeFile  string    `json:"composefile,omitempty"`
	Zip          []byte    `json:"zip,omitempty"`
}

// Stack utilise comme DTO vers le front (lecture de la sortie de commande docker)
type Stack struct {
	Name     string `json:"name,omitempty"`
	Flavour  string `json:"flavour,omitempty"`
	Target   string `json:"target,omitempty"`
}

// Deployment represente un deploiement d'une stack
type Deployment struct {
	ID           string    `json:"id,omitempty"`
	CreationDate time.Time `json:"creationdate,omitempty"`
	Project      string    `json:"project,omitempty"`
	Deployer     string    `json:"deployer,omitempty"`
	Output       string    `json:"output,omitempty"`
}
