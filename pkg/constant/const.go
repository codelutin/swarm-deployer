package constant

import (
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
)

type httpRequestContexttKey string

/*
Nobody l'utilisateur à utiilser s'il n'y a personne d'authentifie
*/
var Nobody = model.User{ID: "nobody"}

/*
User le nom utiliser pour les infos user dans les cookies
*/
const User = "swarm-deployer-user"

/*
Token le nom utiliser pour le token dans les cookies et les parameters de query
*/
const Token = "swarm-deployer-token"

/*
TokenHeader le nom utiliser pour mettre dans le header de requete http (fallback Authorization)
*/
const TokenHeader = "x-" + Token
