// Contains some bits of SwarmKit source code (under Apache-2.0 license)
// Taken from: https://github.com/moby/swarmkit/blob/6675724599d6e6e5ef20e43afad10ee291640cba/manager/controlapi/common.go

package http

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strings"

	"github.com/gorilla/mux"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/constant"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/repository"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"

	"github.com/docker/docker/client"
)

func getProjects(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)
	queryParams := r.URL.Query()
	projectTarget := queryParams.Get("target")
	projectName := queryParams.Get("name")
	projectFlavour := queryParams.Get("flavour")

	json, err := repository.ProjectsJSON(currentUser, projectTarget, projectName, projectFlavour)
	if err != nil {
		if utils.Is404(err) {
			// on a rien retrouve, on renvoie une collection vide
			json = "[]"
		} else {
			utils.Throw(w, err)
			return
		}
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

func projectDelete(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	id := mux.Vars(r)["id"]
	err := repository.ProjectDelete(currentUser, id)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}
}

func getZip(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	id := mux.Vars(r)["id"]
	log.Println("Get ZIP for project", id, "...")
	project, err := repository.Project(currentUser, id)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/zip")
	w.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%v.zip", utils.StackName(project)))
	w.Write(project.Zip)
}

func getProjectsFlavour(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	json, err := repository.ProjectsFlavourJSON(currentUser)
	if err != nil {
		if utils.Is404(err) {
			// on a rien retrouve, on renvoie une collection vide
			json = "[]"
		} else {
			utils.Throw(w, err)
			return
		}
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

func addProjectJSON(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	var project model.Project
	err := json.NewDecoder(r.Body).Decode(&project)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError400(err, currentUser))
		return
	}

	id, err := repository.CreateProject(currentUser, project)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"id": id})
}

func addProject(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	r.ParseMultipartForm(1 * 1024 * 1024) // limit your max input length!
	file, header, err := r.FormFile("zip")
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}
	defer file.Close()

	filename := header.Filename
	filetype := header.Header.Get("Content-Type")
	if !strings.HasSuffix(filename, ".zip") && filetype != "application/zip" {
		utils.Throw(w, utils.NewHTTPError(fmt.Sprintf("Only zip file allowed: '%v' (%v)", filename, filetype), currentUser, 400))
		return
	}

	var buf bytes.Buffer
	buf.ReadFrom(file)
	zip := buf.Bytes()

	name := r.FormValue("name")
	flavour := r.FormValue("flavour")
	target := r.FormValue("target")
	info := r.FormValue("info")
	composeFile := r.FormValue("composeFile")

	var isValidDNSName = regexp.MustCompile(`^[a-zA-Z0-9](?:[-_]*[A-Za-z0-9]+)*$`)

	if !isValidDNSName.MatchString(name) {
		utils.Throw(w, utils.NewHTTPError(fmt.Sprintf("Name '%v' must be valid as a DNS name component", name), currentUser, 400))
		return
	}
	if strings.Contains(name, "--") {
		utils.Throw(w, utils.NewHTTPError(fmt.Sprintf("Name '%v' must not contain '--' (used by swarm-deployer)", name), currentUser, 400))
		return
	}
	if len(name) > 63 {
		// DNS labels are limited to 63 characters
		utils.Throw(w, utils.NewHTTPError(fmt.Sprintf("Name '%v' must be 63 characters or fewer", name), currentUser, 400))
		return
	}

	if name == "" || flavour == "" || target == "" || composeFile == "" {
		utils.Throw(w, utils.NewHTTPError(fmt.Sprintf("All this field is required name: '%v' flavour: '%v' target: '%v' info: '%v' composeFile: '%v'", name, flavour, target, info, composeFile), currentUser, 400))
		return
	}

	project := model.Project{Name: name, Flavour: flavour, Target: target, Info: info, ComposeFile: composeFile, Zip: zip}

	id, err := repository.CreateProject(currentUser, project)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"id": id})
}

// Wrapper around official Docker Go SDK, in order to add our own methods
type DockerClient struct {
	apiClient *client.Client
}

func (dc DockerClient) stacksDeployed(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	json, err := utils.StacksDeployedAsJSON(*dc.apiClient, currentUser)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

func stackServices(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	name := mux.Vars(r)["name"]

	output, err := utils.StackServices(currentUser, name)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"name": name, "output": output})
}

func stackDelete(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	name := mux.Vars(r)["name"]

	output, err := utils.StackDelete(currentUser, name)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(map[string]string{"name": name, "output": output})
}

func deploy(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	id := mux.Vars(r)["id"]
	log.Println("Deploying project", id, "...")
	project, err := repository.Project(currentUser, id)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	output, err := utils.Deploy(currentUser, project)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	deployment := model.Deployment{Project: id, Output: output}

	json, err := repository.CreateDeployment(currentUser, deployment)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError500(err, currentUser))
		return
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}
