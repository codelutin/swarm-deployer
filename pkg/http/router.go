package http

import (
	"context"
	"io"
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/constant"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/repository"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"

	"github.com/gorilla/mux"
	"os"
	"path/filepath"

	"github.com/docker/docker/client"
)

/*
Start web server
*/
func Start(addr string) {
	apiClient, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		panic(err)
	}
	defer apiClient.Close()
	dc := &DockerClient{apiClient: apiClient}

	router := mux.NewRouter()
	// router.Use(logAll)
	router.Use(cors)
	router.Use(authentication)

	// router.HandleFunc("/", handler404)

	s := router.PathPrefix("/api/v1").Subrouter()
	s.HandleFunc("/system/liveness", isAlive).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/users", createUser).Methods(http.MethodPost, http.MethodOptions) // admin
	s.HandleFunc("/users/auth", createAuth).Methods(http.MethodPost, http.MethodOptions)
	s.HandleFunc("/users/{id}", getUser).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/users/{id}/auth", createAuth).Methods(http.MethodPost, http.MethodOptions)
	s.HandleFunc("/users/{id}/auth", deleteAuth).Methods(http.MethodDelete, http.MethodOptions)
	s.HandleFunc("/users/{id}/password", updateUserPassword).Methods(http.MethodPut, http.MethodOptions)
	s.HandleFunc("/users/{id}/token", resetUserToken).Methods(http.MethodPost, http.MethodOptions)
	s.HandleFunc("/projects", addProject).Methods(http.MethodPost, http.MethodOptions)
	s.HandleFunc("/projects", getProjects).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/projects/flavours", getProjectsFlavour).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/projects/{id}/deployments", deploy).Methods(http.MethodPost, http.MethodOptions) // admin
	s.HandleFunc("/projects/{id}/zip", getZip).Methods(http.MethodGet, http.MethodOptions)          // admin
	s.HandleFunc("/projects/{id}", projectDelete).Methods(http.MethodDelete, http.MethodOptions)    // admin
	s.HandleFunc("/stacks", dc.stacksDeployed).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/stacks/{name}", stackServices).Methods(http.MethodGet, http.MethodOptions)
	s.HandleFunc("/stacks/{name}", stackDelete).Methods(http.MethodDelete, http.MethodOptions) // admin

	spa := spaHandler{staticPath: "web", indexPath: "index.html"}
	router.PathPrefix("/").Handler(spa)

	srv := &http.Server{
		Handler: router,
		Addr:    addr,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Fatal(srv.ListenAndServe())
}

func handler404(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "404 page not found", 404)
}

func logAll(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Println("logAll")
		log.Println(r)
		next.ServeHTTP(w, r)
	})
}

func cors(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", r.Header.Get("Origin"))
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		if r.Method == http.MethodOptions {
			log.Println("OPTION request")
			w.Header().Set("Access-Control-Allow-Methods", r.Header.Get("Access-Control-Request-Method"))
			if r.Header.Get("Access-Control-Request-Headers") != "" {
				w.Header().Set("Access-Control-Allow-Headers", strings.Join(r.Header.Values("Access-Control-Request-Headers"), ","))
			}
			return
		}

		next.ServeHTTP(w, r)
	})
}

// je fais comme ca, car je veux garder les URL propre et ne pas introduire de /admin dans certaine (pas uniforme pour le client)
func needAdmin(r *http.Request) bool {
	result := (strings.HasSuffix(r.URL.Path, "/users") && r.Method == http.MethodPost) ||
		(strings.HasSuffix(r.URL.Path, "/deployments") && r.Method == http.MethodPost) ||
		r.Method == http.MethodDelete
	return result
}

func withoutAuthenticationEndpoint(r *http.Request) bool {
	result := !strings.HasPrefix(r.URL.Path, "/api") ||
		strings.HasSuffix(r.URL.Path, "/auth") || // no auth to create/delete auth
		strings.HasSuffix(r.URL.Path, "/system/liveness")
	return result
}

func authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if withoutAuthenticationEndpoint(r) {
			next.ServeHTTP(w, r)
			return
		}

		canBeAppToken := false

		// 1 as query param
		query := r.URL.Query()
		token := query.Get(constant.Token)

		// 2 as header
		if token == "" {
			token = r.Header.Get(constant.TokenHeader)
		} else {
			canBeAppToken = true
		}

		// 3 as Authorization header
		if token == "" {
			tokenBearer := r.Header.Get("Authorization")
			if tokenBearer != "" {
				splitted := strings.Split(tokenBearer, " ") //The token normally comes in format `Bearer {token-body}`, we check if the retrieved token matched this requirement
				if len(splitted) != 2 {
					w.WriteHeader(http.StatusForbidden)
					w.Header().Add("Content-Type", "application/json")
					io.WriteString(w, `{"message": "Invalid/Malformed auth token header Authorization"}`)
					return
				}
				token = splitted[1]
			}
		}

		// 4 as cookie
		if token == "" {
			cookie, err := r.Cookie(constant.Token)
			if err == nil {
				token = cookie.Value
			}
		}

		var user model.User
		var err error

		if token != "" {
			// try as jwt token first
			user, err = utils.JwtVerify(token)
			if err != nil && canBeAppToken {
				// try as application token
				user, err = repository.UserFromToken(token)
				if err != nil {
					utils.Throw(w, utils.NewHTTPError500(err, constant.Nobody))
					return
				}
			} else if err != nil {
				utils.Throw(w, err)
				return
			}
		} else {
			user = constant.Nobody
		}

		log.Printf("User is '%s'\n", user.ID)
		if user == constant.Nobody {
			utils.Throw(w, utils.NewHTTPError("Need authentication", user, 401))
			return
		}

		if !user.Admin && needAdmin(r) {
			utils.Throw(w, utils.NewHTTPError("Need admin", user, 403))
			return
		}

		ctx := context.WithValue(r.Context(), constant.User, user)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r)
	})

}

// spaHandler implements the http.Handler interface, so we can use it
// to respond to HTTP requests. The path to the static directory and
// path to the index file within that static directory are used to
// serve the SPA in the given static directory.
type spaHandler struct {
	staticPath string
	indexPath  string
}

// ServeHTTP inspects the URL path to locate a file within the static dir
// on the SPA handler. If a file is found, it will be served. If not, the
// file located at the index path on the SPA handler will be served. This
// is suitable behavior for serving an SPA (single page application).
func (h spaHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("serve web content", r.URL.Path)
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// prepend the path with the path to the static directory
	path = filepath.Join(h.staticPath, path)

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		// file does not exist, serve index.html
		http.ServeFile(w, r, filepath.Join(h.staticPath, h.indexPath))
		return
	} else if err != nil {
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// otherwise, use http.FileServer to serve the static dir
	http.FileServer(http.Dir(h.staticPath)).ServeHTTP(w, r)
}
