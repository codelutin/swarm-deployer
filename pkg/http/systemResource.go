package http

import (
	"encoding/json"
	"log"
	"net/http"
)

func isAlive(w http.ResponseWriter, r *http.Request) {
	log.Println("http liveness")
	json.NewEncoder(w).Encode(map[string]bool{"alive": true})
}
