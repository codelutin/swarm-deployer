package http

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/constant"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/repository"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/utils"
)

/*
deleteAuth remove cookie authentication
*/
func deleteAuth(w http.ResponseWriter, r *http.Request) {
	cookie := http.Cookie{Name: constant.Token, Value: "", Path: "/", HttpOnly: false, Expires: time.Unix(0, 0)}
	http.SetCookie(w, &cookie)
}

/*
createAuth create JWT token and set header, cookie, body
body: {"token": "xxxx"}
*/
func createAuth(w http.ResponseWriter, r *http.Request) {
	id := mux.Vars(r)["id"]

	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		utils.Throw(w, utils.NewHTTPError400(err, constant.Nobody))
		return
	}

	if id = checkLogin(id, data["email"], data["password"]); id != "" {
		log.Println("create token", data["email"], id)

		pseudoUser := model.User{ID: id}
		userJSON, err := repository.UserJSON(pseudoUser, id)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, pseudoUser))
			return
		}

		var user model.User
		err = json.Unmarshal([]byte(userJSON), &user)
		if err != nil {
			utils.Throw(w, utils.NewHTTPError500(err, pseudoUser))
			return
		}

		token, err := utils.JwtGenerate(user) // si on ajoute plus d'info sensible dans le token, il faudra le chiffrer
		if err != nil {
			utils.Throw(w, err)
			return
		}
		w.Header().Add(constant.TokenHeader, token)

		expiration := time.Now().AddDate(10, 0, 0) // le cookie est valide 10ans :)
		cookieToken := http.Cookie{Name: constant.Token, Value: token, Path: "/", Expires: expiration, HttpOnly: false}
		http.SetCookie(w, &cookieToken)

		w.Header().Add("Content-Type", "application/json")
		io.WriteString(w, userJSON)
	} else {
		utils.Throw(w, utils.NewHTTPError("Bad id or password", constant.Nobody, 403))
	}
}

func checkLogin(id string, email string, password string) string {
	if id != "" && repository.CheckUserPasswordForID(id, password) {
		return id
	}

	result, err := repository.CheckUserPasswordForEmail(email, password)
	if err != nil {
		log.Println("Erreur DB", err)
		return ""
	}

	return result
}

/*
GetUser return all information on user (info, config, auth)
*/
func getUser(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	id := mux.Vars(r)["id"]
	json, err := repository.UserJSON(currentUser, id)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	io.WriteString(w, json)
}

/*
createUser
body: {"login": "toto", "password": "xxxx"}
return: {"id": "[uuid]"}
*/
func createUser(w http.ResponseWriter, r *http.Request) {
	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	adminString := data["admin"]
	admin, err := strconv.ParseBool(adminString)
	if err != nil {
		admin = false
	}

	log.Println("createUser", data["email"], admin)

	id, err := repository.CreateUser(data["email"], data["password"], admin)
	if err != nil {
		utils.Throw(w, err)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"id": id})
}

/*
updateUserPassword
body: {"password": "xxxx", "oldPassword": "yyyy"}
*/
func updateUserPassword(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	id := mux.Vars(r)["id"]
	log.Println("updateUserPassword", id)

	var data map[string]string
	err := json.NewDecoder(r.Body).Decode(&data)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	err = repository.UpdateUserPassword(currentUser, id, data["password"], data["oldPassword"], false)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}
}

/*
resetUserToken
body: {"name": "for application toto", "expiration": 1586081695000}
return: {"token": "uuid"}
*/
func resetUserToken(w http.ResponseWriter, r *http.Request) {
	currentUser := r.Context().Value(constant.User).(model.User)

	id := mux.Vars(r)["id"]

	log.Println("resetUserToken", id)

	token, err := repository.ResetUserToken(currentUser, id)
	if err != nil {
		http.Error(w, fmt.Sprintf("%s", err), 400)
		return
	}

	json.NewEncoder(w).Encode(map[string]string{"token": token})
}
