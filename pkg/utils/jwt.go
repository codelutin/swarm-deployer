package utils

import (
	"github.com/brianvoe/sjwt"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"
)

var secretKey []byte

/*
JwtInit JWT secret key
*/
func JwtInit(key []byte) {
	secretKey = key
}

/*
JwtVerify check token and if valide return user id
*/
func JwtVerify(token string) (model.User, error) {
	var user model.User

	// check signature
	verified := sjwt.Verify(token, secretKey)
	if !verified {
		return user, NewHTTPError("Can't verify token", user, 401)
	}

	// read token
	claims, err := sjwt.Parse(token)
	if err != nil {
		return user, NewHTTPError("Can't read token", user, 401)
	}

	// check date
	err = claims.Validate()
	if err != nil {
		return user, NewHTTPError("Can't validate token", user, 401)
	}

	// userJSON, err := claims.GetStr("user")
	// if err != nil {
	// 	return user, NewHTTPError("Can't get user in token", user, 401)
	// }

	// err = json.Unmarshal([]byte(userJSON), &user)
	// if err != nil {
	// 	return user, NewHTTPError(fmt.Sprintf("Can't unmarshal user in token '%s'", userJSON), user, 401)
	// }

	err = claims.ToStruct(&user)
	if err != nil {
		return user, NewHTTPError("Can't unmarshal token", user, 401)
	}

	return user, nil
}

/*
JwtGenerate generate JWT token from information in parameter
*/
func JwtGenerate(user model.User) (string, error) {
	claims, err := sjwt.ToClaims(user)
	if err != nil {
		return "", NewHTTPError500(err, user)
	}
	// claims := sjwt.New()
	// claims.Set("user", userJSON)
	jwt := claims.Generate(secretKey)

	return jwt, nil
}
