/*
Package utils contient des choses réutilisable
*/
package utils

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

/*
HashPassword return hash for the given password
currently bcrypt is used
*/
func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte([]byte(password)), 8)
	return string(hashedPassword), err
}

/*
CheckPassword is a wrapper around bcrypt.CompareHashAndPassword that returns
true or false
*/
func CheckPassword(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	if err != nil {
		log.Println("CheckPassword: ", err)
		return false
	}

	return true
}
