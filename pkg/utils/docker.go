package utils

import (
	"archive/zip"
	"context"
	"slices"

	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"

	"strings"

	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/constant"
	"gitlab.codelutin.com/codelutin/swarm-deployer/pkg/model"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/client"
)

// Avoid github.com/docker/cli/cli/compose/convert dependency just for this
const (
	LabelNamespace = "com.docker.stack.namespace"
)

// ReadFile lit tout l'ensemble d'un fichier texte et retourne le contenu
func ReadFile(filename string) (string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	return string(content), nil
}

func initFromFile(directory string, prefix string) {
	if directory == "" {
		return
	}

	files, err := ioutil.ReadDir(directory)
	if err != nil {
		log.Printf("Can't read directory '%v'", directory)
		log.Fatal(err)
	}

	for _, filename := range files {
		if strings.HasPrefix(filename.Name(), prefix) {
			content, err := ReadFile(fmt.Sprintf("%v/%v", directory, filename.Name()))
			if err != nil {
				log.Printf("Can't read file content '%v'", filename)
				log.Fatal(err)
			}

			for _, line := range strings.Split(content, "\n") {
				registryInfo := strings.SplitN(line, " ", 3)
				err := login(constant.Nobody, registryInfo[1], registryInfo[2], registryInfo[3])
				if err != nil {
					log.Printf("Can't login to repository '%v' with login '%v'", registryInfo[1], registryInfo[2])
					log.Fatal(err)
				}
			}
		}
	}
}

func initFromVariable(prefix string) {
	splitter := regexp.MustCompile(`(=| )`)
	for _, v := range os.Environ() {
		if strings.HasPrefix(v, prefix) {
			registryInfo := splitter.Split(v, 4)
			err := login(constant.Nobody, registryInfo[1], registryInfo[2], registryInfo[3])
			if err != nil {
				log.Fatal(err, registryInfo)
			}
		}
	}
}

/*
InitDocker force l'authentification sur chaque registry declaree
*/
func InitDocker(prefix string, configRegistryDirectory string) {
	initFromVariable(prefix)
	initFromFile(configRegistryDirectory, prefix)
}

// StackName genere le nom de la stack en fonction du projet
func StackName(project model.Project) string {
	return fmt.Sprintf("%v--%v--%v", project.Target, project.Name, project.Flavour)
}

// StacksDeployedAsJSON liste les stacks deployées actuellement (format JSON)
func StacksDeployedAsJSON(apiClient client.Client, currentUser model.User) (string, error) {
	// Stacks are determined using the service labels, see https://github.com/docker/cli/blob/v27.5.0/cli/command/stack/swarm/list.go

	args := filters.NewArgs()
	args.Add("label", LabelNamespace)

	services, err := apiClient.ServiceList(
		context.Background(),
		types.ServiceListOptions{Filters: args})
	if err != nil {
		return "", NewHTTPError500(err, currentUser)
	}

	seen := []string{}
	stacks := []model.Stack{}

	for _, service := range services {
		labels := service.Spec.Labels

		name, ok := labels[LabelNamespace]
		if !ok {
			msg := fmt.Sprintf("cannot get label %s for service %s", LabelNamespace, service.ID)
			return "", NewHTTPError500WithMessage(msg, err, currentUser)
		}

		if !slices.Contains(seen, name) {
			seen = append(seen, name)
			res := strings.Split(name, "--")
			stack := model.Stack{
				Target:  res[0],
				Name:    res[1],
				Flavour: res[2],
			}
			stacks = append(stacks, stack)
		}
	}

	resultAsJSON, err := json.Marshal(stacks)
	if err != nil {
		return "", NewHTTPError500(err, currentUser)
	}

	result := string(resultAsJSON)
	log.Printf("StacksDeployedAsJSON result: '%v'\n", result)

	return result, nil
}

// StackServices list les services d'une stack swarm
func StackServices(currentUser model.User, name string) (string, error) {
	cmd := exec.Command("docker", "stack", "services", name)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", NewHTTPError500WithMessage(string(output), err, currentUser)
	}
	return string(output), nil
}

// StackDelete supprime une stack du swarm
func StackDelete(currentUser model.User, name string) (string, error) {
	cmd := exec.Command("docker", "stack", "rm", name)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", NewHTTPError500WithMessage(string(output), err, currentUser)
	}
	return string(output), nil
}

func login(currentUser model.User, registryURL string, registryLogin string, registryPassword string) error {
	log.Printf("Docker login for '%v' with login '%v'", registryURL, registryLogin)
	cmd := exec.Command("docker", "login", registryURL, "-u", registryLogin, "-p", registryPassword)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return NewHTTPError500WithMessage(string(output), err, currentUser)
	}

	return nil
}

// Deploy deploie une stack swarm et retourne l'output et les alertes qu'il faudra lever
func Deploy(currentUser model.User, project model.Project) (string, error) {
	// creation d'un rep temporaire
	globalTmpDir := os.Getenv("TMP_DIR")
	if globalTmpDir == "" {
		globalTmpDir = "/tmp"
	}
	tmpdir, err := ioutil.TempDir(globalTmpDir, "deployment")
	// suppression cree pour le decompression du zip
	defer os.RemoveAll(tmpdir)

	zipFilename := fmt.Sprintf("%v/%v.zip", tmpdir, project.ID)
	err = ioutil.WriteFile(zipFilename, project.Zip, 0644)
	// dezip du zip dedans
	err = Unzip(zipFilename, tmpdir)
	if err != nil {
		return "", NewHTTPError500(err, currentUser)
	}

	composeFilePath := fmt.Sprintf("%v/%v", tmpdir, project.ComposeFile)
	stackName := StackName(project)
	cmd := exec.Command("docker", "stack", "deploy", "--with-registry-auth", "--detach", "--compose-file", composeFilePath, stackName)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", NewHTTPError500WithMessage(string(output), err, currentUser)
	}
	log.Println("deploy output", string(output))

	sshResult := sshClient.Push(tmpdir, stackName)

	result := fmt.Sprintf("%v\n\n%v", string(output), sshResult)
	// var output bytes.Buffer
	// cli := command.NewDockerCli(os.Stdin, output, output)
	// cli.Initialize(flags.NewClientOptions())
	// cmd := stack.NewStackCommand(cli)

	// // the command package will pick up these, but you could override if you need to
	// cmd.SetArgs([]string{"deploy", "--compose-file", project.ComposeFile, StackName(project)})
	// cmd.Execute()

	return result, nil
}

// Unzip decompresse un fichier dans un repertoire
func Unzip(src string, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer func() {
		if err := r.Close(); err != nil {
			log.Println("Can't close zip file", src, err)
		}
	}()

	// Closure to address file descriptors issue with all the deferred .Close() methods
	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				log.Println("Can't close file", err)
			}
		}()

		path := filepath.Join(dest, f.Name)

		if f.FileInfo().IsDir() {
			os.MkdirAll(path, f.Mode())
		} else {
			os.MkdirAll(filepath.Dir(path), f.Mode())
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := f.Close(); err != nil {
					log.Println("Can't close file", err)
				}
			}()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}

	for _, f := range r.File {
		err := extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}
