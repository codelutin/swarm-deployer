package utils

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"

	"golang.org/x/crypto/ssh"
)

var sshClient *SSHClient
var prometheusConfigName string

// SSHClient structure permettant d'envoyer facilement des fichiers sur un serveur
type SSHClient struct {
	isInit       bool
	host         string
	targetDir    string
	clientConfig *ssh.ClientConfig
}

// InitSSHClient initialise le client ssh pour de futur usage
// configName le nom du fichier de config prometheus a rechercher, par defaut 'swarm-deployer.prom.yaml'
func InitSSHClient(uri string, keyPath string, configName string) {
	var err error
	sshClient, err = CreateSSHClient(uri, keyPath)
	if err != nil {
		log.Fatal("Can't create ssh client for ", uri, err)
	}

	prometheusConfigName = configName
	if prometheusConfigName == "" {
		prometheusConfigName = "swarm-deployer.prom.yaml"
	}
}

// CreateSSHClient cree un client ssh prend a l'emploi
// uri ex: "ssh://toto@truc.com:2222/config/prom"
func CreateSSHClient(uri string, keyPath string) (*SSHClient, error) {
	uninitializedSSHClient := &SSHClient{isInit: false}
	re := regexp.MustCompile(`^ssh://([^@]+)@([\w.]+(?::[0-9]+)?)(.*)$`)
	if !re.MatchString(uri) {
		return uninitializedSSHClient, nil
	}

	match := re.FindSubmatch([]byte(uri))

	username := string(match[1])
	host := string(match[2])
	targetDir := string(match[3])

	privateKey, err := ioutil.ReadFile(keyPath)

	if err != nil {
		return uninitializedSSHClient, err
	}

	signer, err := ssh.ParsePrivateKey(privateKey)

	if err != nil {
		return uninitializedSSHClient, err
	}

	return &SSHClient{
		isInit:    true,
		host:      host,
		targetDir: targetDir,
		clientConfig: &ssh.ClientConfig{
			User: username,
			Auth: []ssh.AuthMethod{
				ssh.PublicKeys(signer),
			},
		},
	}, nil
}

// Push copy local file to remote ssh directory and use destName on remote
func (c *SSHClient) Push(dirName string, destName string) string {
	promFilePath := fmt.Sprintf("%v/%s", dirName, prometheusConfigName)

	var result string
	file, err := os.Open(promFilePath)
	defer file.Close()
	if err == nil {
		stat, _ := file.Stat()
		result = c.copy(file, stat.Size(), destName)
		if result == "" {
			result = "Configuration prometheus envoyé avec succes"
		}
	} else {
		// pas de fichier de config
		result = c.Delete(destName)
		if result == "" {
			result = "Configuration prometheus supprimé avec succes"
		}
	}
	return result
}

// Delete delete file on remote directory
func (c *SSHClient) Delete(destName string) string {
	return c.copy(nil, 0, destName)
}

// copy local file to remote ssh directory and use destName on remote
func (c *SSHClient) copy(r io.Reader, size int64, destName string) string {
	if !c.isInit {
		return "Pas de configuration SSH pour envoyer le fichier de configuration prometheus"
	}

	client, err := ssh.Dial("tcp", c.host, c.clientConfig)
	if err != nil {
		return fmt.Sprintf("Failed to dial: %v", err.Error())
	}

	session, err := client.NewSession()
	if err != nil {
		return fmt.Sprintf("Failed to create session: %v", err.Error())
	}
	defer session.Close()

	directory := c.targetDir

	go func() {
		w, _ := session.StdinPipe()
		defer w.Close()
		fmt.Fprintln(w, "C0655", size, fmt.Sprintf("%s.yaml", destName))
		if r != nil {
			io.Copy(w, r)
		}
		fmt.Fprintln(w, "\x00")
	}()

	session.Run("/usr/bin/scp -t " + directory)

	return ""
}
